<?php

namespace Otten\WarehouseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="Otten\WarehouseBundle\Repository\WarehouseRepository")
 * @ORM\Table(name="warehouse")
 */
class Warehouse
{
    const CODE_AVAILABLE_ONLINE = 'available-online';
    const CODE_REPAIR = 'repair';
    const CODE_RETURN = 'return';
    const CODE_PHOTO_STUDIO = 'photo-studio';
    const CODE_EXHIBITION = 'exhibition';
    const CODE_HOLD = 'hold';
    const CODE_SHOWCASE_STORE = 'showcase-store';
    const CODE_CONSIGNMENT = 'consignment';
    const CODE_AVAILABLE = 'available';
    const TYPE_WAREHOUSE = 'warehouse';
    const TYPE_STUDIO = 'studio';
    const TYPE_PACKING = 'packing';
    const CHILDREN_COUNT = 0;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @JMS\Groups({"WarehouseList", "WarehouseShow", "StockInboundRequestList", "StockInboundRequestShow", "StockOutboundRequestList", "StockOutboundRequestShow",
     *      "StockMovementList", "StockMovementShow", "StockList", "SettingShow", "StockShow"
     *  })
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Stock", mappedBy="warehouse", cascade={"persist"}, orphanRemoval=true)
     *
     * @JMS\Exclude()
     */
    private $stocks;
}
