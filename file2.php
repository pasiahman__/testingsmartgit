<?php

namespace Otten\ApiBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type as FormType;
use Symfony\Bridge\Doctrine\Form\Type as DoctrineType;

class WarehouseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options = [])
    {
        $builder
            ->add('name')
            ->add('code')
            ->add('type')
            ->add('key', FormType\TextType::class)
            ->add('parent')
            ->add('availableForDelivery', FormType\CheckboxType::class)
            ->add('availableForOrderReturn', FormType\CheckboxType::class)
            ->add('address', FormType\TextType::class)
            ->add('region', DoctrineType\EntityType::class, [
                'class' => 'OttenAppBundle:Region',
            ])
            ->add('latitude', FormType\TextType::class)
            ->add('longitude', FormType\TextType::class)
        ;
    }
}

